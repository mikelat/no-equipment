local entity_types = {}

if settings.startup["neg-locomotive-setting"].value then
    table.insert(entity_types, "locomotive")
end

if settings.startup["neg-cargo-wagon-setting"].value then
    table.insert(entity_types, "cargo-wagon")
end

if settings.startup["neg-fluid-wagon-setting"].value then
    table.insert(entity_types, "fluid-wagon")
end

if settings.startup["neg-artillery-wagon-setting"].value then
    table.insert(entity_types, "artillery-wagon")
end

if settings.startup["neg-spider-vehicle-setting"].value then
    table.insert(entity_types, "spider-vehicle")
end

if settings.startup["neg-car-setting"].value then
    table.insert(entity_types, "car")
end

for _, type in pairs(entity_types) do
    for entity, _ in pairs(data.raw[type]) do
        data.raw[type][entity].equipment_grid = nil
    end
end