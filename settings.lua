data:extend({
    {
    type = "bool-setting",
    name = "neg-locomotive-setting",
    setting_type = "startup",
    default_value = false,
    order = "1",
  },
  {
    type = "bool-setting",
    name = "neg-cargo-wagon-setting",
    setting_type = "startup",
    default_value = true,
    order = "2",
  },
  {
    type = "bool-setting",
    name = "neg-fluid-wagon-setting",
    setting_type = "startup",
    default_value = true,
    order = "3",
  },
  {
    type = "bool-setting",
    name = "neg-artillery-wagon-setting",
    setting_type = "startup",
    default_value = true,
    order = "4",
  },
  {
    type = "bool-setting",
    name = "neg-spider-vehicle-setting",
    setting_type = "startup",
    default_value = false,
    order = "5",
  },
  {
    type = "bool-setting",
    name = "neg-car-setting",
    setting_type = "startup",
    default_value = false,
    order = "6",
  }
})